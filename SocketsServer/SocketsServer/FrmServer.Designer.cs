﻿namespace SocketsServer {
    partial class FrmServer {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnListen = new System.Windows.Forms.Button();
            this.grpReceive = new System.Windows.Forms.GroupBox();
            this.rchReceive = new System.Windows.Forms.RichTextBox();
            this.grpSend = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cboClientIP = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.rchSend = new System.Windows.Forms.RichTextBox();
            this.btnCloseServer = new System.Windows.Forms.Button();
            this.grpReceive.SuspendLayout();
            this.grpSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("幼圆", 9F);
            this.txtPort.Location = new System.Drawing.Point(370, 12);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(187, 24);
            this.txtPort.TabIndex = 11;
            this.txtPort.Text = "8045";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("黑体", 12F);
            this.label2.Location = new System.Drawing.Point(315, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "PORT";
            // 
            // txtAddr
            // 
            this.txtAddr.Font = new System.Drawing.Font("幼圆", 9F);
            this.txtAddr.Location = new System.Drawing.Point(98, 12);
            this.txtAddr.Name = "txtAddr";
            this.txtAddr.Size = new System.Drawing.Size(187, 24);
            this.txtAddr.TabIndex = 9;
            this.txtAddr.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("黑体", 12F);
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "ADDRESS";
            // 
            // btnListen
            // 
            this.btnListen.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnListen.Location = new System.Drawing.Point(576, 12);
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new System.Drawing.Size(99, 25);
            this.btnListen.TabIndex = 12;
            this.btnListen.Text = "启动监听";
            this.btnListen.UseVisualStyleBackColor = true;
            this.btnListen.Click += new System.EventHandler(this.btnListen_Click);
            // 
            // grpReceive
            // 
            this.grpReceive.Controls.Add(this.rchReceive);
            this.grpReceive.Location = new System.Drawing.Point(12, 43);
            this.grpReceive.Name = "grpReceive";
            this.grpReceive.Size = new System.Drawing.Size(751, 261);
            this.grpReceive.TabIndex = 13;
            this.grpReceive.TabStop = false;
            this.grpReceive.Text = "接收区";
            // 
            // rchReceive
            // 
            this.rchReceive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rchReceive.Font = new System.Drawing.Font("幼圆", 9F);
            this.rchReceive.Location = new System.Drawing.Point(3, 21);
            this.rchReceive.Name = "rchReceive";
            this.rchReceive.Size = new System.Drawing.Size(745, 237);
            this.rchReceive.TabIndex = 0;
            this.rchReceive.Text = "";
            // 
            // grpSend
            // 
            this.grpSend.Controls.Add(this.btnCancel);
            this.grpSend.Controls.Add(this.cboClientIP);
            this.grpSend.Controls.Add(this.label3);
            this.grpSend.Controls.Add(this.btnSend);
            this.grpSend.Controls.Add(this.rchSend);
            this.grpSend.Location = new System.Drawing.Point(12, 310);
            this.grpSend.Name = "grpSend";
            this.grpSend.Size = new System.Drawing.Size(751, 199);
            this.grpSend.TabIndex = 14;
            this.grpSend.TabStop = false;
            this.grpSend.Text = "发送区";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnCancel.Location = new System.Drawing.Point(456, 24);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(119, 27);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "清空发送框";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboClientIP
            // 
            this.cboClientIP.FormattingEnabled = true;
            this.cboClientIP.Location = new System.Drawing.Point(95, 24);
            this.cboClientIP.Name = "cboClientIP";
            this.cboClientIP.Size = new System.Drawing.Size(210, 23);
            this.cboClientIP.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("黑体", 10F);
            this.label3.Location = new System.Drawing.Point(9, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "客户端IP";
            // 
            // btnSend
            // 
            this.btnSend.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnSend.Location = new System.Drawing.Point(336, 24);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(100, 27);
            this.btnSend.TabIndex = 3;
            this.btnSend.Text = "发  送";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // rchSend
            // 
            this.rchSend.Font = new System.Drawing.Font("幼圆", 9F);
            this.rchSend.Location = new System.Drawing.Point(3, 63);
            this.rchSend.Name = "rchSend";
            this.rchSend.Size = new System.Drawing.Size(739, 130);
            this.rchSend.TabIndex = 0;
            this.rchSend.Text = "";
            this.rchSend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rchSend_KeyDown);
            // 
            // btnCloseServer
            // 
            this.btnCloseServer.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnCloseServer.Location = new System.Drawing.Point(681, 12);
            this.btnCloseServer.Name = "btnCloseServer";
            this.btnCloseServer.Size = new System.Drawing.Size(77, 25);
            this.btnCloseServer.TabIndex = 15;
            this.btnCloseServer.Text = "关  闭";
            this.btnCloseServer.UseVisualStyleBackColor = true;
            this.btnCloseServer.Click += new System.EventHandler(this.btnCloseServer_Click);
            // 
            // FrmServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(777, 521);
            this.Controls.Add(this.btnCloseServer);
            this.Controls.Add(this.grpSend);
            this.Controls.Add(this.grpReceive);
            this.Controls.Add(this.btnListen);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAddr);
            this.Controls.Add(this.label1);
            this.Name = "FrmServer";
            this.Text = "服务器端界面";
            this.grpReceive.ResumeLayout(false);
            this.grpSend.ResumeLayout(false);
            this.grpSend.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnListen;
        private System.Windows.Forms.GroupBox grpReceive;
        private System.Windows.Forms.RichTextBox rchReceive;
        private System.Windows.Forms.GroupBox grpSend;
        private System.Windows.Forms.ComboBox cboClientIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox rchSend;
        private System.Windows.Forms.Button btnCloseServer;
        private System.Windows.Forms.Button btnCancel;
    }
}

