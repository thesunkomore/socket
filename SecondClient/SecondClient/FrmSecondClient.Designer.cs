﻿namespace SecondClient {
    partial class FrmClient {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpReceive = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.rchReceive = new System.Windows.Forms.RichTextBox();
            this.grpSend = new System.Windows.Forms.GroupBox();
            this.btnCancelSend = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.rchSend = new System.Windows.Forms.RichTextBox();
            this.grpReceive.SuspendLayout();
            this.grpSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtPort.Font = new System.Drawing.Font("幼圆", 9F);
            this.txtPort.Location = new System.Drawing.Point(489, 11);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(187, 24);
            this.txtPort.TabIndex = 15;
            this.txtPort.Text = "8045";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("黑体", 12F);
            this.label2.Location = new System.Drawing.Point(434, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "PORT";
            // 
            // txtAddr
            // 
            this.txtAddr.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddr.Font = new System.Drawing.Font("幼圆", 9F);
            this.txtAddr.Location = new System.Drawing.Point(165, 12);
            this.txtAddr.Name = "txtAddr";
            this.txtAddr.Size = new System.Drawing.Size(187, 24);
            this.txtAddr.TabIndex = 13;
            this.txtAddr.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("黑体", 12F);
            this.label1.Location = new System.Drawing.Point(80, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "ADDRESS";
            // 
            // grpReceive
            // 
            this.grpReceive.Controls.Add(this.btnClose);
            this.grpReceive.Controls.Add(this.btnConnect);
            this.grpReceive.Controls.Add(this.rchReceive);
            this.grpReceive.Location = new System.Drawing.Point(12, 42);
            this.grpReceive.Name = "grpReceive";
            this.grpReceive.Size = new System.Drawing.Size(751, 261);
            this.grpReceive.TabIndex = 16;
            this.grpReceive.TabStop = false;
            this.grpReceive.Text = "接收区";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnClose.Location = new System.Drawing.Point(603, 147);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 40);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "关  闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnConnect.Location = new System.Drawing.Point(603, 64);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(120, 40);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "连  接";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // rchReceive
            // 
            this.rchReceive.Font = new System.Drawing.Font("幼圆", 9F);
            this.rchReceive.Location = new System.Drawing.Point(7, 25);
            this.rchReceive.Name = "rchReceive";
            this.rchReceive.Size = new System.Drawing.Size(590, 219);
            this.rchReceive.TabIndex = 0;
            this.rchReceive.Text = "";
            // 
            // grpSend
            // 
            this.grpSend.Controls.Add(this.btnCancelSend);
            this.grpSend.Controls.Add(this.btnSend);
            this.grpSend.Controls.Add(this.rchSend);
            this.grpSend.Location = new System.Drawing.Point(12, 309);
            this.grpSend.Name = "grpSend";
            this.grpSend.Size = new System.Drawing.Size(751, 172);
            this.grpSend.TabIndex = 17;
            this.grpSend.TabStop = false;
            this.grpSend.Text = "发送区";
            // 
            // btnCancelSend
            // 
            this.btnCancelSend.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnCancelSend.Location = new System.Drawing.Point(603, 99);
            this.btnCancelSend.Name = "btnCancelSend";
            this.btnCancelSend.Size = new System.Drawing.Size(120, 40);
            this.btnCancelSend.TabIndex = 4;
            this.btnCancelSend.Text = "清空发送框";
            this.btnCancelSend.UseVisualStyleBackColor = true;
            this.btnCancelSend.Click += new System.EventHandler(this.btnCancelSend_Click);
            // 
            // btnSend
            // 
            this.btnSend.Font = new System.Drawing.Font("幼圆", 9F);
            this.btnSend.Location = new System.Drawing.Point(603, 39);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(120, 40);
            this.btnSend.TabIndex = 3;
            this.btnSend.Text = "发  送";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // rchSend
            // 
            this.rchSend.Font = new System.Drawing.Font("幼圆", 9F);
            this.rchSend.Location = new System.Drawing.Point(7, 25);
            this.rchSend.Name = "rchSend";
            this.rchSend.Size = new System.Drawing.Size(590, 129);
            this.rchSend.TabIndex = 0;
            this.rchSend.Text = "";
            this.rchSend.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rchSend_KeyDown);
            // 
            // FrmClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(788, 500);
            this.Controls.Add(this.grpSend);
            this.Controls.Add(this.grpReceive);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAddr);
            this.Controls.Add(this.label1);
            this.Name = "FrmClient";
            this.Text = "第二个客户端界面";
            this.Load += new System.EventHandler(this.FrmClient_Load);
            this.grpReceive.ResumeLayout(false);
            this.grpSend.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpReceive;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.RichTextBox rchReceive;
        private System.Windows.Forms.GroupBox grpSend;
        private System.Windows.Forms.Button btnCancelSend;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox rchSend;
    }
}

